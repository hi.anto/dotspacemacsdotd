(add-hook 'org-mode-hook
          (lambda ()
            (org-mode-visual-fill/set)
            (visual-fill-column-mode 1)))
