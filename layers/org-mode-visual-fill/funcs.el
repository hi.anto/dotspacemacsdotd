(defun org-mode-visual-fill/set ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-line-mode 1)
  (org-indent-mode))

(defun org-mode-visual-fill/unset ()
  (setq visual-fill-column-width nil
        visual-fill-column-center-text nil)
  (visual-line-mode nil)
  (org-indent-mode))
