(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."
  (setq powerline-default-separator 'nil)
  (org-roam-db-autosync-enable)

  (setq scss-output-directory "../css")

  (add-hook 'python-mode-hook 'anaconda-mode)

  (add-hook 'web-mode-hook 'emmet-mode)
  (add-hook 'css-mode-hook 'emmet-mode))
